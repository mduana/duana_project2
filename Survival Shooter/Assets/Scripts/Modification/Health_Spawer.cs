﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health_Spawer : MonoBehaviour
{
    public GameObject[] pickupPrefabs;
    GameObject spawnedPickup;
    float waitToSpawn = 0f;
    void Start()
    {
        SpawnPickup();
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnedPickup == null)
        {
            if(waitToSpawn <= 0f)
                SpawnPickup();
            else
                waitToSpawn -= Time.deltaTime;

            print("waittoSpawn: " + waitToSpawn);
        }

    }
    void SpawnPickup()
    {
        GameObject toSpawn = pickupPrefabs[(int)(Random.value * pickupPrefabs.Length)];
        spawnedPickup = Instantiate(toSpawn, transform.position, toSpawn.transform.rotation) as GameObject;
        waitToSpawn = Random.Range(2f, 4f);
    }
}